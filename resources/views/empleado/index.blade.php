{{--https://www.youtube.com/watch?v=9DU7WLZeam8&t=1065s--}}
@extends('layouts.app')

@section('content')
<div class="container">


    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{Session::get('mensaje')}}
        @endif
        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>--}}

    </div>


    <a href="{{url('empleado/create')}}" class="btn btn-success">Registrar empleado</a>

    <table class="table table-light">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Correo</th>
                <th>Empresa</th>
                <th>Acciones</th>
             

            </tr>
        </thead>
        <tbody>
            @foreach($empleados as $empleado)
            
            <tr>
                <td>{{$empleado->id}}</td>

                <td>
                    {{--<img class="img-thumbnail img-fluid" src="{{asset('storage'.'/'.$empleado->Foto)}}" width="100" alt="foto">--}}
                    <img class="img-thumbnail img-fluid" src="{{asset($empleado->Foto)}}" width="100" alt="foto">
                </td>


                <td>{{$empleado->Nombre}}</td>
                <td>{{$empleado->ApellidoPaterno}}</td>

                <td>{{$empleado->ApellidoMaterno}}</td>

                <td>{{$empleado->Correo}}</td>

                <td>
                    {{$empleado->empresas->Nombre}}
                </td>
                <td>
                    <a href="{{url('/empleado/'.$empleado->id.'/edit')}}" class="btn btn-warning">Editar</a>
                    
                    |
                    
                    <form action="{{url('/empleado/'.$empleado->id )}}" class="d-inline" method="post">
                        @csrf
                        {{method_field('DELETE')}}
                        <input type="submit" class="btn btn-danger" onclick="return confirm('¿Quieres borrar?')" value="Borrar">
                    </form>
                </td>
                
            </tr>
              
            @endforeach

        </tbody>
    </table>

</div>
@endsection