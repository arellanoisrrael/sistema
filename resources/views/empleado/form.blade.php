<h1>{{$modo}} empleado </h1>

@if(count($errors)>0)
<div class="alert alert-danger" role="alert">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-group">
    <Label for="Nombre">Nombre</Label>
    <input type="text" class="form-control" name="Nombre" value="{{isset($empleado->Nombre)?$empleado->Nombre:old('Nombre')}}" id="Nombre">
</div>

<div class="form-group">
    <Label for="ApellidoPaterno">Apellido Paterno</Label>
    <input type="text" class="form-control" name="ApellidoPaterno" value="{{isset($empleado->ApellidoPaterno)?$empleado->ApellidoPaterno:old('ApellidoPaterno')}}" id="ApellidoPaterno">
</div>

<div class="form-group">
    <Label for="ApellidoMaterno">Apellido Materno</Label>
    <input type="text" class="form-control" name="ApellidoMaterno" value="{{isset($empleado->ApellidoMaterno)?$empleado->ApellidoMaterno:old('ApellidoMaterno')}}" id="ApellidoMaterno">
</div>

<div class="form-group">
    <Label for="Correo">Correo</Label>
    <input type="text" class="form-control" name="Correo" value="{{isset($empleado->Correo)?$empleado->Correo:old('Correo')}}" id="Correo">
</div>

<div class="form-group">
    <Label for="Empresa">Empresa</Label>
    <select class="form-control form-select" name="Empresa" id="Empresa">
        @foreach($empresas as $empresa)
        <option value="{{$empresa->id}}">{{$empresa->Nombre}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <Label for="Foto">Foto</Label>
    @if(isset($empleado->Foto))
    {{--{{$empleado->Foto}}--}}
    {{--<img class="img-thumbnail img-fluid" src="{{asset('storage'.'/'.$empleado->Foto)}}" width="100" alt="foto">--}}
    <img class="img-thumbnail img-fluid" src="{{asset($empleado->Foto)}}" width="100" alt="foto">
    @endif
    <input type="file" class="form-control" name="Foto" value="{{$modo}}" id="Foto">
</div>

<input class="btn btn-success" type="submit" value="{{$modo}} datos">
<a class="btn btn-primary" href="{{url('empleado/')}}">Regresar</a>
<br>