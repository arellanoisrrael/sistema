{{--https://www.youtube.com/watch?v=9DU7WLZeam8&t=1065s--}}

{{--Mostrar la listra de empresas--}}
@extends('layouts.app')

@section('content')
<div class="container">
    
    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{Session::get('mensaje')}}
        @endif
        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>--}}

    </div>


    <a href="{{url('empresa/create')}}" class="btn btn-success">Registrar empresa</a>

    <table class="table table-light">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Orden</th>
                <th>Estado</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody>
            @foreach($empresas as $empresa)
            <tr>
                <td>{{$empresa->id}}</td>
                <td>{{$empresa->Nombre}}</td>
                <td>{{$empresa->Orden}}</td>

                <td>{{$empresa->Estado}}</td> 
                <td>
                    <a href="{{url('/empresa/'.$empresa->id.'/edit')}}" class="btn btn-warning">Editar</a>
                    
                    |
                    
                    <form action="{{url('/empresa/'.$empresa->id )}}" class="d-inline" method="post">
                        @csrf
                        {{method_field('DELETE')}}
                        <input type="submit" class="btn btn-danger" onclick="return confirm('¿Quieres borrar?')" value="Borrar">
                    </form>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

</div>
@endsection