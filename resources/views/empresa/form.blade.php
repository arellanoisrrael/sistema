{{--Formulario que tendrá los datos en común con create y edit --}}

<h1>{{$modo}} empresa </h1>

@if(count($errors)>0)
<div class="alert alert-danger" role="alert">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif



<div class="form-group">
    <Label for="Nombre">Nombre</Label>
    <input type="text" class="form-control" name="Nombre" value="{{isset($empresa->Nombre)?$empresa->Nombre:old('Nombre')}}" id="Nombre">
</div>

<div class="form-group">
    <Label for="Orden">Orden</Label>
    <input type="text" class="form-control" name="Orden" value="{{isset($empresa->Orden)?$empresa->Orden:old('Orden')}}" id="Orden">
</div>

<div class="form-group">
    <Label for="Estado">Estado</Label>
    <input type="text" class="form-control" name="Estado" value="{{isset($empresa->Estado)?$empresa->Estado:old('Estado')}}" id="Estado">
</div>



<input class="btn btn-success" type="submit" value="{{$modo}} datos">
<a class="btn btn-primary" href="{{url('empresa/')}}">Regresar</a>
<br>