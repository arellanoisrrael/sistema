<?php

namespace App\Http\Controllers;


use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Empleado;
use Image;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['empleados'] = Empleado::all(); //para consultar los 5 registros
       // $empresas= Empleado::all();

        return view('empleado.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas= Empresa::WHERE("Estado", "Activo")->get();
        return view('empleado.create', compact("empresas")); /*le estamos dando el controlador la información de la vista*/

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        /*Validaciones*/
        $campos = [
            'Nombre' => 'required|string|max:100',
            'ApellidoPaterno' => 'required|string|max:100',
            'ApellidoMaterno' => 'required|string|max:100',
            'Correo' => 'required|email',
            'Empresa'=> 'required'
            

            //'Foto'=>'required|max:10000|mimes:jpeg,jpg, png',
        ];
        $mensaje = [
            'required' => 'El :attributo es requerido',
            //'Foto.required'=>'La foto es requerida'
        ];
        // unimos las validaciones
        $this->validate($request, $campos, $mensaje);
        /*Fin Validaciones*/

        //retornamos los datos enviados
        $datosEmpleado = request()->except('_token', 'Empresa');

        //aaa
        $nombre = date('Ymdhis') . '.' . $request->file('Foto')->extension(); //agregar el nombre a

        $ruta = storage_path() . '/app/public/uploads/' . $nombre;  // para saber la ruta acceso directo
        $productImg = 'storage/uploads/' . $nombre;  // carpeta donde se guarda la imagen

        Image::make($request->file('Foto'))->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($ruta);

        $datosEmpleado['Foto'] = $productImg;
        $datosEmpleado['id_empresa']= $request->Empresa;
        //return $ruta;
        //Image::make($request->hasFile('Foto'))->save($ruta);
        // fin aaa


        //para subir la foto ala DB
        /*if($request->hasFile('Foto')){
            $datosEmpleado['Foto']=$request->file('Foto')->store('uploads','public');
        }*/

        Empleado::insert($datosEmpleado); //inserta a la base de datos exceptuando el token

        // return response()->json($datosEmpleado);
        return redirect('empleado')->with('mensaje', 'Empleado Agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = Empleado::findOrFail($id); //buscamos la información a partir del id 
        $empresas= Empresa::WHERE("Estado", "Activo")->get();
        return view('empleado.edit', compact('empleado','empresas')); // retornamos la vista pasandole la información 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*Validaciones*/
        $campos = [
            'Nombre' => 'required|string|max:100',
            'ApellidoPaterno' => 'required|string|max:100',
            'ApellidoMaterno' => 'required|string|max:100',
            'Correo' => 'required|email',

        ];
        $mensaje = [
            'required' => 'El :attribute es requerido',

        ];
        //
        if ($request->hasFile('Foto')) {
            $campos = ['Foto' => 'required|max:10000|mimes:jpg, png,jpeg'];
            $mensaje = ['Foto.required' => 'La foto es requerida'];
        }
        // unimos las validaciones
        $this->validate($request, $campos, $mensaje);
        /*Fin Validaciones*/


        //retornamoes los datos enviados
        $datosEmpleado = request()->except(['_token', '_method', 'Empresa']);

        $empleado = Empleado::findOrFail($id); //buscamos la información a partir del id 
        //para subir la foto ala DB
        if ($request->hasFile('Foto')) {

            
            $url = str_replace('storage', 'public', $empleado->Foto); //reemplaza storagae por plublic
            Storage::delete($url);
            //$datosEmpleado['Foto'] = $request->file('Foto')->store('uploads', 'public');

            $nombre = date('Ymdhis') . '.' . $request->file('Foto')->extension(); //agregar el nombre a

            $ruta = storage_path() . '/app/public/uploads/' . $nombre;  // para saber la ruta donde se guarda 
            $productImg = 'storage/uploads/' . $nombre;  // carpeta donde se guarda la imagen  la ruta acceso directo

            Image::make($request->file('Foto'))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($ruta);

            $datosEmpleado['Foto'] = $productImg; /// esto se guarda en la base de datos
        }
        else {
            $datosEmpleado['Foto'] =  $empleado->Foto; // en caso que no exista asigan la imagen anterior
        }

        Empleado::where('id', '=', $id)->update($datosEmpleado);

        $empleado = Empleado::findOrFail($id); //buscamos la información a partir del id 
        // return view('empleado.edit', compact('empleado')); // retornamos la vista pasandole la información

        return redirect('empleado')->with('mensaje', 'Empleado Modificado ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $empleado = Empleado::findOrFail($id);
        $url = str_replace('storage', 'public', $empleado->Foto); //reemplaza storagae por plublic
        //dd($empleado->Foto);
        if (Storage::delete($url)) { //si existe la foto lo borramos
            Empleado::destroy($id);
            return redirect('empleado')->with('mensaje', 'Empleado Borrado '); //respues del borrado para que se redireccione al url de empleado
        } else {
            return redirect('empleado')->with('mensaje', 'No se pudo eliminar '); //respues del borrado para que se redireccione al url de empleado
        }
    }
}
