<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Empleado;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['empresas'] = Empresa::all(); //para consultar los registros
       // $empresas= Empleado::all();

        return view('empresa.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          /*Validaciones*/
          $campos = [
            
            'Nombre' => 'required|string|max:100',
            'Orden' => 'required|integer|max:100',
            'Estado' => 'required|string',
            
            

            //'Foto'=>'required|max:10000|mimes:jpeg,jpg, png',
        ];
        $mensaje = [
            'required' => 'El :attribute es requerido',
            //'Foto.required'=>'La foto es requerida'
        ];
        // unimos las validaciones
        $this->validate($request, $campos, $mensaje);
        /*Fin Validaciones*/

        //retornamos los datos enviados
        $datosEmpresa = request()->except('_token');


        Empresa::insert($datosEmpresa); //inserta a la base de datos exceptuando el token

        // return response()->json($datosEmpresa);
        return redirect('empresa')->with('mensaje', 'Empresa Agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa $empresa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa $empresa)
    {
        $empresa = Empresa::findOrFail($empresa->id); //buscamos la información a partir del id 
        return view('empresa.edit', compact('empresa')); // retornamos la vista pasandole la información 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresa $empresa)
    {
         /*Validaciones*/
         $campos = [
            
            'Nombre' => 'required|string|max:100',
            'Orden' => 'required|integer|max:100',
            'Estado' => 'required|string',
            

        ];
        $mensaje = [
            'required' => 'El :attribute es requerido',

        ];
        //
        
        // unimos las validaciones
        $this->validate($request, $campos, $mensaje);
        /*Fin Validaciones*/


        //retornamoes los datos enviados
        $datosEmpresa = request()->except(['_token', '_method']);
 
        Empresa::where('id', '=', $empresa->id)->update($datosEmpresa);


        return redirect('empresa')->with('mensaje', 'Empresa Modificado ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresa $empresa)
    {
       
        Empresa::destroy($empresa->id);
        return redirect('empresa')->with('mensaje', 'Empresa Borrado '); //respues del borrado para que se redireccione al url de empresa
    }
}
