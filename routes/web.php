<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpleadoController;
use App\Http\Controllers\EmpresaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('auth.login');
});

//
/*
Route::get('/empleado', function () {
    return view('empleado.index');
});
Route::get('/empleado/create',[EmpleadoController::class, 'create']); //Puedo acceder solo a create*/
Route::resource('empleado', EmpleadoController::class)->middleware('auth'); //con esto podemos acceder a todas las rutas
Auth::routes();
Route::resource('empresa',EmpresaController::class)->middleware('auth'); // accedemos a todas las rutas de empresa con esta única sentencia


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware'=>'auth'],function(){
    Route::get('/',[EmpleadoController::class, 'index'])->name('home');
});


